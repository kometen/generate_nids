use serde_json::json;
use worker::*;
use crate::norwegian_id::generate_synthetic_birthdate;

mod utils;
mod models;
mod norwegian_id;

fn log_request(req: &Request) {
    console_log!(
        "{} - [{}], located at: {:?}, within: {}",
        Date::now().to_string(),
        req.path(),
        req.cf().coordinates().unwrap_or_default(),
        req.cf().region().unwrap_or_else(|| "unknown region".into())
    );
}

#[event(fetch)]
pub async fn main(req: Request, env: Env, _ctx: worker::Context) -> Result<Response> {
    log_request(&req);

    // Optionally, get more helpful error messages written to the console in the case of a panic.
    utils::set_panic_hook();

    // Optionally, use the Router to handle matching endpoints, use ":name" placeholders, or "*name"
    // catch-all's to match on specific patterns. Alternatively, use `Router::with_data(D)` to
    // provide arbitrary data that will be accessible in each route via the `ctx.data()` method.
    let router = Router::new();

    // Add as many routes as your Worker needs! Each route will get a `Request` for handling HTTP
    // functionality and a `RouteContext` which you can use to  and get route parameters and
    // Environment bindings like KV Stores, Durable Objects, Secrets, and Variables.
    router
        .get("/", |_, _| Response::ok("Hello from Workers!"))

        .get_async("/no/:items", |_req, ctx| async move {
            let i = get_items(ctx);
            if i > 0 && i < 100 {
                return Response::from_json(&json!(generate_synthetic_birthdate(i)))
            } else {
                Response::error("Bad Request", 400)
            }
        })

        .post_async("/form/:field", |mut req, ctx| async move {
            if let Some(name) = ctx.param("field") {
                let form = req.form_data().await?;
                return match form.get(name) {
                    Some(FormEntry::Field(value)) => {
                        Response::from_json(&json!({ name: value }))
                    }
                    Some(FormEntry::File(_)) => {
                        Response::error("`field` param in form shouldn't be a File", 422)
                    }
                    None => Response::error("Bad Request", 400),
                }
            }

            Response::error("Bad Request", 400)
        })
        .get("/worker-version", |_, ctx| {
            let version = ctx.var("WORKERS_RS_VERSION")?.to_string();
            Response::ok(version)
        })
        .run(req, env)
        .await
}

fn get_items(ctx: RouteContext<()> ) -> u32 {
    if let Some(items) = ctx.param("items") {
        match items.parse::<u32>() {
            Ok(n) => n,
            Err(_) => 0
        }
    } else {
        0
    }
}