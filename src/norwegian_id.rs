use std::collections::HashMap;
use std::ops::Add;
use rand::Rng;
use time::util::is_leap_year;
use crate::models::Nid;

// synthetic_month is month-number + 80. Feb. is 82, October 90 etc.
pub(crate) fn generate_synthetic_birthdate(items: u32) -> Vec<Nid> {
    let mut rng = rand::thread_rng();
    let mut nids: Vec<Nid> = Vec::new();
    let mut birth_date_map: HashMap<String, i32> = HashMap::new();
    let mut count: u32 = 0;

    let items = if items > 10000 {
        9999
    } else { items
    };

    let country_code = "NO".to_string();
    let region = "Norway".to_string();

    while count < items {
        // Get year now so we can check for leap-year in February.
        let year = rng.gen_range(1854..2040);
        let year_str = format!("{}", year);
        let first_digit_in_decade = year_str.chars().take(3).last().unwrap();
        let second_digit_in_decade = year_str.chars().take(4).last().unwrap();
        let decade = format!("{}{}", first_digit_in_decade, second_digit_in_decade);
        //println!("year: {}, decade: {}", year, decade);

        let synthetic_month: i32 = rng.gen_range(81..93);
        let date = match synthetic_month {
            81 | 83 | 85 | 87 | 88 | 90 | 92 => {
                //println!("31 days");
                format!("{:0>2}", rng.gen_range(1..32))
            },
            82 => {
                let leap = is_leap_year(year);
                let days = if leap {
                    29
                } else {
                    28
                };
                //println!("{} days", &days);
                format!("{:0>2}", rng.gen_range(1..days))
            },
            _ => {
                //println!("30 days");
                format!("{:0>2}", rng.gen_range(1..31))
            }
        };

        let bd = &date.clone().add(&*synthetic_month.to_string()).add(&*year.to_string());
        let (serial, status) = match year {
            1854..=1899 => {
                let start = 750;
                let mut end = 500;
                let serial = birth_date_map.entry(bd.to_string()).or_insert(start);
                *serial -= 1;
                let status = if &serial >= &&mut end {
                    "ok".to_string()
                } else {
                    "circular".to_string()
                };
                (serial, status)
            },
            1940..=1999 => {
                let start = 1000;
                let mut end = 900;
                let serial = birth_date_map.entry(bd.to_string()).or_insert(start);
                *serial -= 1;
                let status = if &serial >= &&mut end {
                    "ok".to_string()
                } else {
                    "circular".to_string()
                };
                (serial, status)
            },
            1900..=1999 => {
                let start = 500;
                let mut end = 0;
                let serial = birth_date_map.entry(bd.to_string()).or_insert(start);
                *serial -= 1;
                let status = if &serial >= &&mut end {
                    "ok".to_string()
                } else {
                    "circular".to_string()
                };
                (serial, status)
            },
            _ => {
                let start = 1000;
                let mut end = 500;
                let serial = birth_date_map.entry(bd.to_string()).or_insert(start);
                *serial -= 1;
                let status = if &serial >= &&mut end {
                    "ok".to_string()
                } else {
                    "circular".to_string()
                };
                (serial, status)
            }
        };

        let serial = &date.add(&*synthetic_month.to_string()).add(&*decade.to_string()).add(&*format!("{:0>3}", serial));
        //println!("serial: {}", serial);

        let checksum1 = match calculate_checksum_1(serial) {
            None => {
                //eprintln!("Invalid checksum for serial {}", serial.to_string());
                "x".to_string()
            }
            Some(checksum) => {
                //println!("checksum1: {}", checksum);
                checksum
            }
        };

        if !checksum1.eq(&"10".to_string()) {
            let serial =  serial.clone().add(checksum1.as_str());
            //println!("serial: {}", serial);

            let checksum2 = match calculate_checksum_2(&serial) {
                None => {
                    //eprintln!("Invalid checksum for serial {}", serial.to_string());
                    "x".to_string()
                }
                Some(checksum) => {
                    //println!("checksum2: {}", checksum);
                    checksum
                }
            };

            if !checksum2.eq(&"10".to_string()) {
                let serial =  serial.clone().add(checksum2.as_str());
                //println!("serial: {}", serial);

                let nid = Nid {
                    country_code: country_code.clone(),
                    region: region.clone(),
                    synthetic: true,
                    status,
                    id: serial.to_string()
                };

                nids.push(nid);
                count += 1;
            }
        }
    }
    nids
}

fn calculate_checksum_1(serial: &String) -> Option<String> {
    let s: Vec<_> = serial.chars().collect();
    if s.len() != 9 {
        None
    } else {
        let checksum = 11 - ((
            s[0].to_digit(10).unwrap() * 3 +
            s[1].to_digit(10).unwrap() * 7 +
            s[2].to_digit(10).unwrap() * 6 +
            s[3].to_digit(10).unwrap() +
            s[4].to_digit(10).unwrap() * 8 +
            s[5].to_digit(10).unwrap() * 9 +
            s[6].to_digit(10).unwrap() * 4 +
            s[7].to_digit(10).unwrap() * 5 +
            s[8].to_digit(10).unwrap() * 2) % 11
        );
        if checksum == 11 {
            Some("0".to_string())
        } else {
            Some(checksum.to_string())
        }
    }
}

fn calculate_checksum_2(serial: &String) -> Option<String> {
    let s: Vec<_> = serial.chars().collect();
    if s.len() != 10 {
        None
    } else {
        let checksum = 11 - ((
            s[0].to_digit(10).unwrap() * 5 +
            s[1].to_digit(10).unwrap() * 4 +
            s[2].to_digit(10).unwrap() * 3 +
            s[3].to_digit(10).unwrap() * 2+
            s[4].to_digit(10).unwrap() * 7 +
            s[5].to_digit(10).unwrap() * 6 +
            s[6].to_digit(10).unwrap() * 5 +
            s[7].to_digit(10).unwrap() * 4 +
            s[8].to_digit(10).unwrap() * 3 +
            s[9].to_digit(10).unwrap() * 2) % 11
        );
        if checksum == 11 {
            Some("0".to_string())
        } else {
            Some(checksum.to_string())
        }
    }
}
